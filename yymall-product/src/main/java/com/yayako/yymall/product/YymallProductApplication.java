package com.yayako.yymall.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YymallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(YymallProductApplication.class, args);
    }
}
