package com.yayako.yymall.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YymallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(YymallOrderApplication.class, args);
    }
}
