package com.yayako.yymall.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YymallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(YymallWareApplication.class, args);
    }
}
