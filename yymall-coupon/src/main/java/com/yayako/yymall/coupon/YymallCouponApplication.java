package com.yayako.yymall.coupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YymallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(YymallCouponApplication.class, args);
    }
}
