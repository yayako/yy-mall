package com.yayako.yymall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YymallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(YymallMemberApplication.class, args);
    }
}
